var PayslipGenerator = require('payslip-generator')
var taxTable         = require('./tax-table')
var payslip          = new PayslipGenerator(taxTable)

var source = [
  'David,Rudd,60050,9,March',
  'Ryan,Chen,120000,10,March'
]

source.forEach(printPayslip)

function printPayslip(data) {
  var input         = data.split(',')
  var firstName     = input[0]
  var lastName      = input[1]
  var annualSalary  = parseFloat(input[2])
  var superRate     = parseFloat(input[3])
  var paymentPeriod = input[4]

  var result = payslip.generate(firstName, lastName, annualSalary, superRate, paymentPeriod)

  console.log(result[1])
  console.log('-----------------------')
}
